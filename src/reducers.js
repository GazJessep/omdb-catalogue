//
//  Reducer composition
//  probably not much point here but...
//
function searchTerm(state = '', action) {
  switch (action.type) {
    case 'UPDATE_SEARCH_TERM':
      return action.payload
    default:
      return state
  }
}

function currentSearchTerm(state = '', action) {
  switch (action.type) {
    case 'UPDATE_CURRENT_SEARCH_TERM':
      return action.payload
    default:
      return state
  }
}

function searchResults(state = [], action) {
  switch (action.type) {
    case 'UPDATE_SEARCH_RESULTS':
      return action.payload
    default:
      return state
  }
}

function resultsPageIndex(state = 1, action) {
  switch (action.type) {
    case 'UPDATE_RESULTS_PAGE_INDEX':
      return action.payload
    default:
      return state
  }
}

function resultsPages(state = 0, action) {
  switch (action.type) {
    case 'UPDATE_RESULTS_PAGES':
      return action.payload
    default:
      return state
  }
}

function favourites(state = [], action) {
  switch(action.type) {
    case 'UPDATE_FAVOURITES':
      return action.payload
    default:
      return state
  }
}

function loading(state = false, action) {
  switch(action.type) {
    case 'CHANGE_LOADING':
      return action.payload
    default:
      return state
  }
}

function diceApp(state = {}, action) {
  return {
    searchTerm: searchTerm(state.searchTerm, action),
    currentSearchTerm: currentSearchTerm(state.currentSearchTerm, action),
    searchResults: searchResults(state.searchResults, action),
    resultsPageIndex: resultsPageIndex(state.resultsPageIndex, action),
    resultsPages: resultsPages(state.resultsPages, action),
    favourites: favourites(state.favourites, action)
  }
}

export default diceApp;
