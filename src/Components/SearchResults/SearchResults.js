import React, { Component } from 'react';
import './SearchResults.less';
import { MediaList } from '../MediaList/MediaList';

import {
  updateSearchTerm,
  updateSearchResults,
  updateResultsPageIndex,
  updateResultsPages,
  updateFavourites
} from '../../actions';
import diceApp from '../../reducers';

class SearchResults extends Component {
	constructor(props) {
		super(props);
		this.handlePagPrev = this.handlePagPrev.bind(this);
		this.handlePagNext = this.handlePagNext.bind(this);

		this.currentState = this.props.store.getState();
		this.currentResults = this.props.store.getState().searchResults;
		this.unsubscribe = this.props.store.subscribe(() => {
			if (this.props.store.getState().searchResults !== this.currentResults || this.props.store.getState().loading !== this.currentState.loading) {
				// hacky force render as state has changed
				this.setState({});
			}
    })
		this.componentWillUnmount = function() {
			this.unsubscribe();
		}
	}

	handlePagPrev() {
		this.props.store.dispatch(updateResultsPageIndex(this.props.store.getState().resultsPageIndex - 1));
	}
	handlePagNext() {
		this.props.store.dispatch(updateResultsPageIndex(this.props.store.getState().resultsPageIndex + 1));
	}

	render() {

		this.currentIndex = this.props.store.getState().resultsPageIndex;
		this.pagesCount = this.props.store.getState().resultsPages;
		this.prevDisabled = this.currentIndex > 1 ? false : 'disabled';
		this.nextDisabled = this.currentIndex < this.pagesCount ? false : 'disabled';

		if (this.props.store.getState().loading) {
			console.log('loading')
			return (
				<div className="search-results loading">
					<h2>Loading</h2>
				</div>
			)
		} else if (this.props.store.getState().searchTerm && this.props.store.getState().searchResults.length) {
			return (
				<div className="search-results">
					<ul className="search-results__list">
						{
							this.props.store.getState().searchResults.map(media => <MediaList media={media} key={'id-' + media.id} store={this.props.store} />)
						}
					</ul>
					<button disabled={this.prevDisabled} onClick={this.handlePagPrev} >&lt;</button>
					<button disabled={this.nextDisabled} onClick={this.handlePagNext} >&gt;</button>
				</div>
			);
		} else if (!this.props.store.getState().searchTerm.length) {
			return (
				<div className="search-results">
					<h2>What are you looking for today? Type it in the box above :)</h2>
				</div>
			);
		} else {
			return (
				<div className="search-results">
					<h2>No results for {this.props.store.getState().searchTerm} :(</h2>
				</div>
			);
		}


	}
}

export { SearchResults };
