import React, { Component } from 'react';
import './SearchBar.less';

import {
  updateSearchTerm,
	updateCurrentSearchTerm,
  updateSearchResults,
  updateResultsPageIndex,
  updateResultsPages,
  updateFavourites,
	changeLoadingStatus
} from '../../actions';
import diceApp from '../../reducers';

import { TheMoviedb } from '../../TheMoviedb';

class SearchBar extends Component {
	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.search = this.search.bind(this);
		this.searchTerm = this.props.store.getState().searchTerm;
		this.resultsPageIndex = this.props.store.getState().resultsPageIndex;

		if (this.props.match.params.query && !this.props.store.getState().searchTerm) {
			this.props.store.dispatch(updateSearchTerm(this.props.match.params.query));
			this.search();
		}

		//	clear search when hitting homepage
    if (this.props.match.path === '/') {
      this.props.store.dispatch(updateSearchTerm(''));
      this.props.store.dispatch(updateSearchResults([]));
      this.props.store.dispatch(updateResultsPages(0));
    }

		//	fired any time store updated
		this.unsubscribe = this.props.store.subscribe(() => {
			if (this.props.store.getState().resultsPageIndex !== this.resultsPageIndex) {
				console.log('updateResultsPageIndex mismatch:', this.resultsPageIndex, this.props.store.getState().resultsPageIndex);
				this.resultsPageIndex = this.props.store.getState().resultsPageIndex;
				this.search();
			}
    })
		this.componentWillUnmount = function() {
			this.unsubscribe();
		}

	}

	handleChange(event) {
		this.props.store.dispatch(updateSearchTerm(event.target.value));
	}

  search(event) {
		if (event) event.preventDefault();

//    this.searchTerm = this.props.store.getState().searchTerm;
    if (!this.props.store.getState().searchTerm.trim().length) {
      return;
    } else {
			//	whenever doing a new search (doesn't match url), reset pagination to first page
			if (this.props.store.getState().currentSearchTerm !== this.props.store.getState().searchTerm) {
				console.log('searchTerm mismatch:', this.props.store.getState().currentSearchTerm, this.props.store.getState().searchTerm);
				this.props.store.dispatch(updateResultsPageIndex(1));
				this.props.store.dispatch(updateCurrentSearchTerm(this.props.store.getState().searchTerm));
			}
      this.searchTerm = encodeURI(this.props.store.getState().currentSearchTerm);
			this.props.store.dispatch(changeLoadingStatus(true));
      TheMoviedb.search(this.searchTerm, this.props.store.getState().resultsPageIndex)
        .then(results => {
          this.props.store.dispatch(updateSearchResults(results.items));
          this.props.store.dispatch(updateResultsPages(results.total_pages));
					this.props.store.dispatch(changeLoadingStatus(false));
        }
      );
    }
		this.props.history.push(`/search/${this.searchTerm}`);
  }

	render() {
		return (
			<form className="search-bar" onSubmit={this.search}>
				<input className="search-bar__field" type="text" placeholder="Search for a Movie..." defaultValue={this.props.store.getState().searchTerm}
				 onChange={this.handleChange} />
			 <button className="search-bar__button material-icons" type="submit">search</button>
			</form>
		);
	}
}

export { SearchBar };
