import React, { Component } from 'react';
import './MediaList.less';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';

class MediaList extends Component {
	constructor(props) {
		super(props);
		this.store = this.props.store;
		this.handleFav = this.handleFav.bind(this);
	}

	handleFav() {
		this.props.onFav(this.props.media);
	}

	isFav() {
		return this.store.getState().favourites.filter(m => m.id === this.props.media.id).length === 1
	}

	render() {
		const media = this.props.media;
		return (
			<li className="media">
				<Link to={`/media/${media.id}`} >
					<img className="media__thumb" src={media.poster || ""} alt={media.title} />
					<h4 className="media__title">{media.title}</h4>
					<p className="media__meta">
						<span className="genres">{media.genres}</span>
						<span className="runtime">{media.runtime}</span>
						<span className={`favourite favourite-${this.isFav()} material-icons`}>{this.isFav() ? 'favorite' : 'favorite_border'}</span>
					</p>
				</Link>

			</li>
		);
	}
}

export { MediaList };
