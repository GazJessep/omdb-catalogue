import React, { Component } from 'react';
// import './FavouritesList.css';

import { MediaList } from '../MediaList/MediaList';

class FavouritesList extends Component {
	constructor(props) {
		super(props);

		//	this.methodName = this.methodName.bind(this);
	}

	render() {
		return (
			<div className="search-results">
				<h2>Favourites</h2>
				<ul className="search-results__list">
					{
						this.props.store.getState().favourites.map(media => <MediaList media={media} key={'id-' + media.id} store={this.props.store} />)
					}
				</ul>
			</div>
		);
	}
}

export { FavouritesList };
