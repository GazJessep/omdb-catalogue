import React, { Component } from 'react';



// Router
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';

import './App.less';

//  Components
import { Page } from '../Page/Page';
import { FavouritesList } from '../FavouritesList/FavouritesList';
import { MediaDetail } from '../MediaDetail/MediaDetail';

//  Store
import { createStore } from 'redux';
import diceApp from '../../reducers';
const store = createStore(
  diceApp,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Router>
        <div className="app">
          <Route path="/"
            exact
            render={(props) => <Page {...props}
            store={store} />}
          />

          <Route path="/search/:query"
            render={(props) => <Page {...props}
            store={store} />}
          />

          <Route path="/media/:id"
            render={(props) => <Page {...props}
            store={store}/>}
          />

          <Route path="/favourites"
            render={(props) => <FavouritesList {...props}
            store={store} />}
          />
        </div>
      </Router>
    );
  }
}
export default App;
