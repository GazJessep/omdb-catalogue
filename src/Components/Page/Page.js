import React, { Component } from 'react';
import './Page.less';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
	WithRouter
} from 'react-router-dom';

import { Navigation } from '../Navigation/Navigation';
import { SearchResults } from '../SearchResults/SearchResults';
import { MediaDetail } from '../MediaDetail/MediaDetail';

class Page extends Component {
	constructor(props) {
		super(props);
	}

	render() {

		return (
			<div>
				<Navigation store={this.props.store} match={this.props.match} history={this.props.history} />

				{
					this.props.store.getState().searchResults && !this.props.match.params.id && <SearchResults store={this.props.store} match={this.props.match} />
				}

        {
          this.props.match.params.id &&
          <MediaDetail store={this.props.store} match={this.props.match} />
        }
			</div>
		);
	}
}

export { Page };
