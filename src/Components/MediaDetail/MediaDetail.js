import React, { Component } from 'react';
import './MediaDetail.less';

import { TheMoviedb } from '../../TheMoviedb';

import {
  updateSearchTerm,
  updateSearchResults,
  updateResultsPageIndex,
  updateResultsPages,
  updateFavourites
} from '../../actions';
import diceApp from '../../reducers';

class MediaDetail extends Component {
	constructor(props) {
		super(props);
    this.toggleFav = this.toggleFav.bind(this);
    this.isFav = this.isFav.bind(this);

		this.id = parseInt(this.props.match.params.id);
    this.currentResults = this.props.store.getState().searchResults;

		this.unsubscribe = this.props.store.subscribe(() => {
			if (this.props.store.getState().searchResults !== this.currentResults) {
				// hacky force render as state has changed
				this.setState({});
			}
    })
		this.componentWillUnmount = function() {
			this.unsubscribe();
		}

		if (!this.props.store.getState().searchResults.length) {
			//	navigated directly (or refreshed) -> no search results, lookup by id
			console.log('looking up media by id', this.id);
			TheMoviedb.getMediaById(this.id)
				.then(media => {
					//	if we can't find, take back to start to search
					if (!media.id) {
						this.props.history.push('/');
						return
					}
					this.props.store.dispatch(updateSearchResults([media]))
				});
		}

  }

  toggleFav() {
    let existsIndex;
    let newFavourites = [...this.props.store.getState().favourites];
    if (newFavourites.filter((item, index) => {
        if(item.id === this.media.id) {
          existsIndex = index; return true;
        }
        return false;
      }).length) {
      newFavourites.splice(existsIndex, 1);
    } else { newFavourites.push(this.media) }
    this.props.store.dispatch(updateFavourites(newFavourites));
  }

	isFav() {
		return this.props.store.getState().favourites.filter(m => m.id === this.media.id).length === 1
	}

	render() {
		this.media = this.props.store.getState().searchResults.filter(m => m.id === this.id
    )[0];
		if (this.media) {
			return (
				<div className="media media--detail">
					<img className="media__thumb media--detail__thumb" src={this.media.poster || ""} alt={this.media.title} />
					<h4 className="media__title media--detail__title">{this.media.title}</h4>
					<h5 className="media__tagline media--detail__tagline">{this.media.tagline}</h5>
					<p className="media__meta media--detail__meta">
						<span className="genres">{this.media.genres}</span>
						<span className="runtime">{this.media.runtime}</span>
						<button onClick={this.toggleFav} className={`favourite favourite-${this.isFav()} material-icons`}>{this.isFav() ? 'favorite' : 'favorite_border'}</button>
					</p>
					<p>{this.media.overview}</p>
				</div>
			);
		} else { return null; }
	}
}

export { MediaDetail };
