import React, { Component } from 'react';
import './Navigation.less';

import { SearchBar } from '../SearchBar/SearchBar';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
	WithRouter
} from 'react-router-dom';

import {
  updateSearchTerm,
  updateSearchResults,
  updateResultsPageIndex,
  updateResultsPages,
  updateFavourites
} from '../../actions';
import diceApp from '../../reducers';

class Navigation extends Component {
	constructor(props) {
		super(props);

		this.unsubscribe = this.props.store.subscribe(() => {
			this.setState({})
		})
		this.componentWillUnmount = function() {
			this.unsubscribe();
		}

		//	clear search when hitting homepage
    if (this.props.match.path === '/') {
      this.props.store.dispatch(updateSearchTerm(''));
      this.props.store.dispatch(updateSearchResults([]));
      this.props.store.dispatch(updateResultsPages(0));
			this.props.store.dispatch(updateResultsPageIndex(1));
    }
	}

	render() {
		this.showHome = true; //	this.props.match.url !== '/';
		this.showSearchBar = !this.props.match.params.id;
		this.showSearchResults = this.props.match.path === '/search/:query' || this.props.match.path === '/media/:id' && this.props.store.getState().currentSearchTerm;
		this.showMedia = this.props.match.path === '/media/:id';
		this.showFavourites = this.showHome || this.props.store.getState().favourites;

		return (
			<div className="navigation">

				{
					this.showHome && <Link to="/" className="material-icons">home</Link>
				}
				{
					this.showSearchResults && <Link to={`/search/${this.props.store.getState().currentSearchTerm}`}>
					<span className="material-icons">search</span>
					{this.props.store.getState().currentSearchTerm}
					</Link>
				}
				{
					this.showMedia && this.props.store.getState().searchResults.length && <Link to={`/media/${this.props.match.params.id}`}>
					<span className="material-icons">movie</span>
					{
						this.props.store.getState().searchResults.filter(m => m.id === parseInt(this.props.match.params.id)
					)[0].title
					}
					</Link>
				}
				{
					this.showFavourites && <Link to='/favourites' className="favourites"><span className="material-icons">favorite</span>{this.props.store.getState().favourites.length ? this.props.store.getState().favourites.length : ''}</Link>
				}
				{
					this.showSearchBar && <SearchBar store={this.props.store} match={this.props.match} history={this.props.history} />
				}
			</div>
		);
	}
}

export { Navigation };
