const apiKey = '383a88d8c47ed0a920487236d301530f';
const baseUrl = 'https://api.themoviedb.org/3';

const TheMoviedb = {
  search(query, page) {
    console.log('searching for ', query, page);
    return fetch(`https://cors-anywhere.herokuapp.com/${baseUrl}/search/movie/?query=${query}&api_key=${apiKey}&page=${page}`,
      {"Content-Type": "application/json"}
    )
    .then(response => response.json())
    .then(responseJson => {
    console.log('found', responseJson);
      if (responseJson.total_results) {
        return {
          page: responseJson.page,
          total_pages: responseJson.total_pages,
          items: responseJson.results.map(m => {
            return {
              id:       m.id,
              title:    m.title,
              overview: m.overview,
              genres:   m.genre_ids,
              rating:   m.vote_average,
              poster:   m.poster_path ? `https://image.tmdb.org/t/p/w300_and_h450_bestv2${m.poster_path}` : false
            }
          })
        }
      } else {
        return {
          page: responseJson.page,
          total_pages: responseJson.total_pages,
          items: []
        }
      }
    });
  },

  getMediaById(id) {
    console.log('getting media by id ', id);
    return fetch(`https://cors-anywhere.herokuapp.com/${baseUrl}/movie/${id}?api_key=${apiKey}`,
      {"Content-Type": "application/json"}
    )
    .then(response => response.json())
    .then(responseJson => {
      console.log('found', responseJson, typeof responseJson);
      if (responseJson.id) {
        const m = responseJson;
        return {
          id:       m.id,
          title:    m.title,
          tagline:  m.tagline,
          overview: m.overview,
          genres:   m.genre_ids,
          rating:   m.vote_average,
          poster:   m.poster_path ? `https://image.tmdb.org/t/p/w300_and_h450_bestv2${m.poster_path}` : false
        }
      } else {
        return {}
      }
    });
  }
}

export { TheMoviedb };
