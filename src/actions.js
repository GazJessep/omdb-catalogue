export function updateSearchTerm(searchTerm) {
  return { type: 'UPDATE_SEARCH_TERM', payload: searchTerm }
}
export function updateCurrentSearchTerm(searchTerm) {
  return { type: 'UPDATE_CURRENT_SEARCH_TERM', payload: searchTerm }
}
export function updateSearchResults(searchResults) {
  return { type: 'UPDATE_SEARCH_RESULTS', payload: searchResults }
}
export function updateResultsPageIndex(resultsPageIndex) {
  return { type: 'UPDATE_RESULTS_PAGE_INDEX', payload: resultsPageIndex }
}
export function updateResultsPages(resultsPages) {
  return { type: 'UPDATE_RESULTS_PAGES', payload: resultsPages }
}
export function updateFavourites(favourites) {
  return { type: 'UPDATE_FAVOURITES', payload: favourites }
}
export function changeLoadingStatus(status) {
  return { type: 'CHANGE_LOADING', payload: status }
}
